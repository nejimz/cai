@extends('layouts.app')

@section('page-title', 'Assign Student')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Assign Student</div>

                <div class="card-body">
                    <form action="{{ $route }}" method="post" enctype="multipart/form-data">
                        @csrf()

                        <div class="results">
                            @include('partials.error-msg')
                        </div>

                        <div class="form-group row">
                            <label for="teacher_user_id" class="col-md-2 col-form-label text-md-right">Teacher</label>
                            <div class="col-md-10">
                                <select class="form-control" aria-label="Default select example" id="teacher_user_id" name="teacher_user_id">
                                    <option value="">-- Select --</option>
                                    @foreach($teachers as $teacher)
                                    <option value="{{ $teacher->id }}">{{ $teacher->last_name .', '. $teacher->first_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="student_user_id" class="col-md-2 col-form-label text-md-right">Student</label>
                            <div class="col-md-10">
                                <select class="form-control" aria-label="Default select example" id="student_user_id" name="student_user_id">
                                    <option value="">-- Select --</option>
                                    @foreach($students as $student)
                                    <option value="{{ $student->id }}">{{ $student->last_name .', '. $student->first_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-5 offset-md-2">
                                @if($button_text != 'Save')
                                    @method('PUT')
                                @endif
                                <button type="submit" class="btn btn-primary">{{ $button_text }}</button>
                                <a href="{{ route('teacher.show', Auth::user()->id) }}" class="btn btn-secondary">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('#teacher_user_id').val('{{ $teacher_user_id }}');
        $('#student_user_id').val('{{ $student_user_id }}');
    });
</script>
@endsection
