<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AuthController@login')->name('login');
Route::post('authentication', 'AuthController@authentication')->name('authentication');
Route::post('authentication-api', 'AuthController@authentication_api')->name('authentication_api');
Route::post('authentication-api-logout', 'AuthController@authentication_api_logout')->name('authentication_api_logout');

Route::middleware('auth')->group(function(){
	Route::post('logout', 'AuthController@logout')->name('logout');

	Route::get('home', 'HomeController@index')->name('home');

	Route::resource('words', 'WordsController');

	Route::get('teacher/remove/{id}', 'TeacherController@delete')->name('teacher.remove.student');
	Route::get('teacher/assign-student', 'TeacherController@create_student')->name('teacher.create_student');
	Route::post('teacher/assign-student', 'TeacherController@store_student')->name('teacher.store_student');
	Route::resource('teacher', 'TeacherController');

	Route::get('student/score/{id}', 'StudentController@api_scores')->name('student.scores');
	Route::resource('student', 'StudentController');
});

Route::get('easy', 'ChallengeController@easy')->name('challenge_easy');
Route::get('medium', 'ChallengeController@medium')->name('challenge_medium');
Route::get('hard', 'ChallengeController@hard')->name('challenge_hard');
Route::get('get-result', 'ChallengeController@getResult')->name('challenge_get_result');
Route::post('get-result', 'ChallengeController@getResult')->name('challenge_get_result_post');
